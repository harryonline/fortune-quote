// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Fortune QuoteShow random quote from http://wertarbyte.de/gigaset-rss/ on a Garmin Connect IQ device

using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.System;

var appVersion = "1.1.18";
var reader;
var writer;

class QuoteApp extends App.AppBase {

  function initialize() {
    AppBase.initialize();
    App.getApp().setProperty("appVersion", appVersion);
  }

  // Return the initial view of your application here
  function getInitialView() {
    reader = new QuoteReader();
    writer = new WrapText();
    var settings = System.getDeviceSettings();
    if (!settings.phoneConnected) {
      var message = Ui.loadResource(Rez.Strings.NoConnection);
      return [new MessageView(message)];
    } else {
      return [new SplashView()];
    }
  }

  function getGlanceView() {
    return [ new GlanceView()];
  }
}
