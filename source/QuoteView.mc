// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Show quote on the screen

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System;

var initialLoad = true;

class QuoteView extends Ui.View {

  var quoteFont;
  var sourceFont;

  function initialize() {
    View.initialize();
  }

  // Load your resources here
  function onLayout(dc) {
    setLayout(Rez.Layouts.MainLayout(dc));
    setFontSize(dc);
    if (initialLoad) {
      reader.load();
      initialLoad = false;
    }
  }

  // Update the view
  function onUpdate(dc) {
    View.onUpdate(dc);
    var posY = 0;
    dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK);
    posY = writer.writeLines(dc, reader.quote, quoteFont, posY);
    dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_BLACK);
    posY = writer.writeLines(dc, reader.source, sourceFont, posY);
    writer.testFit(posY);
  }

  function setFontSize(dc) {
    var height = dc.getFontHeight(Gfx.FONT_SMALL);
    if (height < 20) {
      quoteFont = Gfx.FONT_MEDIUM;
      sourceFont = Gfx.FONT_SMALL;
    } else {
      quoteFont = Gfx.FONT_SMALL;
      sourceFont = Gfx.FONT_TINY;
    }
  }

}
