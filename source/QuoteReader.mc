// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Get Quote from RSS feed

using Toybox.Application;
using Toybox.Timer;
using Toybox.WatchUi as Ui;
using Toybox.System;

class QuoteReader {

  var quote ="";
  var source = "";

  hidden var loadedCallback;
  hidden var url;

  function initialize() {
    url = Application.getApp().getProperty("link");
    if (url.equals("")) {
      url = Ui.loadResource(Rez.Strings.DefaultLink);
    }
  }

  function load() {
    Ui.pushView( new LoadingView(), null, 0);
    readRSS(url, method(:loaded));
  }

  function loaded(status, data) {
    if( status == 200 ) {
        quote = data["quote"];
        source = data["source"];
    } else {
        quote = "Error " + status;
        source = errorMessage(status);
    }
    Ui.popView(0);
  }
}
