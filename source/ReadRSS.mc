// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Read content of an RSS feed

using Toybox.Communications;

function readRSS( url, callback) {
  // Do not use this url other apps, it may change without notice
  // Instead, create your own, can use the source of this:
  // https://gitlab.com/harryonline/rss2json/blob/master/index.js
  // var rss2json = "https://u1n2xc1lke.execute-api.us-west-2.amazonaws.com/prod/xml2json";
  var rss2json = "https://3vrlupl2b6.execute-api.eu-west-1.amazonaws.com/prod/xml2json";
  var parameters = {
    "rss_url" => url
  };
  var options = {
    :method => Communications.HTTP_REQUEST_METHOD_GET
  };
  Communications.makeWebRequest( rss2json, parameters, options, callback);
}
